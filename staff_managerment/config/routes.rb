Rails.application.routes.draw do
  # namespace :admin do
  #     resources :admins

  #     root to: "admins#index"
  #   end
  devise_for :admins, :skip => [:sessions]

  as :admin do
    get 'admins/sign_in' => 'devise/sessions#new', :as => :new_admin_session
    post 'sessions/admin' => 'devise/sessions#create', :as => :admin_session
    delete 'admins/sign_out' => 'devise/sessions#destroy', :as => :destroy_admin_session
  end
  get 'sessions/new'
  resources :acounts
  #resources :staffs
  resources :staff_skills
  resources :skills
  resources :positions
  resources :projects
  resources :framworks
  resources :teams
  resources :sessions 
  resources :staffs, :path => "information"

  root 'skills#page_home'
  # get    '/login' => 'sessions#new'
  # post   '/login' => 'sessions#create'
  # delete '/logout' => 'sessions#destroy'
  
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  # admin skill (add)
  get '/skill' => 'skills#index'
  get '/skill/1' => 'skills#show'
  post '/skill/new' => 'skills#create'
  post '/skill/1/edit' => 'skills#edit'

  get '/project' => 'projects#index'
  get '/project/1' => 'projects#show'
  post '/project/new' => 'projects#create'
  post '/project/1/edit' => 'projects#edit'

  get '/team' => 'teams#index'
  get '/team/1' => 'teams#show'
  post '/team/new' => 'teams#create'
  post '/team/1/edit' => 'teams#edit'

  get '/position' => 'positions#index'
  get '/position/1' => 'teapositionsms#show'
  post '/position/new' => 'positions#create'
  post '/position/1/edit' => 'positions#edit'

  get '/framwork' => 'framworks#index'
  get '/framwork/1' => 'framworks#show'
  post '/framwork/new' => 'framworks#create'
  post '/framwork/1/edit' => 'framworks#edit'

  #get '/staff' => 'staffs#index'
  get '/information/1' => 'staffs#show'
  post '/information/new' => 'staffs#create'
  post '/information/1/edit' => 'staffs#edit'
  get '/information' => 'staffs#page_home'
  post '/information' => 'staffs#search'
  #get '/staff/1/detail' => 'staffs#detail'

  get '/staff_skill' => 'staff_skills#index'
  get '/staff_skill/1' => 'staff_skills#show'
  post '/staff_skill/new' => 'staff_skills#create'
  post '/staff_skill/1/edit' => 'staff_skills#edit'

 
end
