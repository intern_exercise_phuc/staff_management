require 'test_helper'

class StaffSkillsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @staff_skill = staff_skills(:one)
  end

  test "should get index" do
    get staff_skills_url
    assert_response :success
  end

  test "should get new" do
    get new_staff_skill_url
    assert_response :success
  end

  test "should create staff_skill" do
    assert_difference('StaffSkill.count') do
      post staff_skills_url, params: { staff_skill: { skill_id: @staff_skill.skill_id, staff_id: @staff_skill.staff_id } }
    end

    assert_redirected_to staff_skill_url(StaffSkill.last)
  end

  test "should show staff_skill" do
    get staff_skill_url(@staff_skill)
    assert_response :success
  end

  test "should get edit" do
    get edit_staff_skill_url(@staff_skill)
    assert_response :success
  end

  test "should update staff_skill" do
    patch staff_skill_url(@staff_skill), params: { staff_skill: { skill_id: @staff_skill.skill_id, staff_id: @staff_skill.staff_id } }
    assert_redirected_to staff_skill_url(@staff_skill)
  end

  test "should destroy staff_skill" do
    assert_difference('StaffSkill.count', -1) do
      delete staff_skill_url(@staff_skill)
    end

    assert_redirected_to staff_skills_url
  end
end
