require 'test_helper'

class FramworksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @framwork = framworks(:one)
  end

  test "should get index" do
    get framworks_url
    assert_response :success
  end

  test "should get new" do
    get new_framwork_url
    assert_response :success
  end

  test "should create framwork" do
    assert_difference('Framwork.count') do
      post framworks_url, params: { framwork: { name: @framwork.name } }
    end

    assert_redirected_to framwork_url(Framwork.last)
  end

  test "should show framwork" do
    get framwork_url(@framwork)
    assert_response :success
  end

  test "should get edit" do
    get edit_framwork_url(@framwork)
    assert_response :success
  end

  test "should update framwork" do
    patch framwork_url(@framwork), params: { framwork: { name: @framwork.name } }
    assert_redirected_to framwork_url(@framwork)
  end

  test "should destroy framwork" do
    assert_difference('Framwork.count', -1) do
      delete framwork_url(@framwork)
    end

    assert_redirected_to framworks_url
  end
end
