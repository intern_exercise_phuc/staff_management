require "application_system_test_case"

class AcountsTest < ApplicationSystemTestCase
  setup do
    @acount = acounts(:one)
  end

  test "visiting the index" do
    visit acounts_url
    assert_selector "h1", text: "Acounts"
  end

  test "creating a Acount" do
    visit acounts_url
    click_on "New Acount"

    fill_in "Password", with: @acount.password
    fill_in "Username", with: @acount.username
    click_on "Create Acount"

    assert_text "Acount was successfully created"
    click_on "Back"
  end

  test "updating a Acount" do
    visit acounts_url
    click_on "Edit", match: :first

    fill_in "Password", with: @acount.password
    fill_in "Username", with: @acount.username
    click_on "Update Acount"

    assert_text "Acount was successfully updated"
    click_on "Back"
  end

  test "destroying a Acount" do
    visit acounts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Acount was successfully destroyed"
  end
end
