require "application_system_test_case"

class FramworksTest < ApplicationSystemTestCase
  setup do
    @framwork = framworks(:one)
  end

  test "visiting the index" do
    visit framworks_url
    assert_selector "h1", text: "Framworks"
  end

  test "creating a Framwork" do
    visit framworks_url
    click_on "New Framwork"

    fill_in "Name", with: @framwork.name
    click_on "Create Framwork"

    assert_text "Framwork was successfully created"
    click_on "Back"
  end

  test "updating a Framwork" do
    visit framworks_url
    click_on "Edit", match: :first

    fill_in "Name", with: @framwork.name
    click_on "Update Framwork"

    assert_text "Framwork was successfully updated"
    click_on "Back"
  end

  test "destroying a Framwork" do
    visit framworks_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Framwork was successfully destroyed"
  end
end
