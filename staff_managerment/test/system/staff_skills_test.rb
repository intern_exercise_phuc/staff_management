require "application_system_test_case"

class StaffSkillsTest < ApplicationSystemTestCase
  setup do
    @staff_skill = staff_skills(:one)
  end

  test "visiting the index" do
    visit staff_skills_url
    assert_selector "h1", text: "Staff Skills"
  end

  test "creating a Staff skill" do
    visit staff_skills_url
    click_on "New Staff Skill"

    fill_in "Skill", with: @staff_skill.skill_id
    fill_in "Staff", with: @staff_skill.staff_id
    click_on "Create Staff skill"

    assert_text "Staff skill was successfully created"
    click_on "Back"
  end

  test "updating a Staff skill" do
    visit staff_skills_url
    click_on "Edit", match: :first

    fill_in "Skill", with: @staff_skill.skill_id
    fill_in "Staff", with: @staff_skill.staff_id
    click_on "Update Staff skill"

    assert_text "Staff skill was successfully updated"
    click_on "Back"
  end

  test "destroying a Staff skill" do
    visit staff_skills_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Staff skill was successfully destroyed"
  end
end
