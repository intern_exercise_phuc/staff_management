-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 10, 2019 at 08:48 AM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `staff_managerment_development`
--

-- --------------------------------------------------------

--
-- Table structure for table `acounts`
--

CREATE TABLE `acounts` (
  `id` bigint(20) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ar_internal_metadata`
--

CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_internal_metadata`
--

INSERT INTO `ar_internal_metadata` (`key`, `value`, `created_at`, `updated_at`) VALUES
('environment', 'development', '2019-07-09 02:45:30.986788', '2019-07-09 02:45:30.986788');

-- --------------------------------------------------------

--
-- Table structure for table `framworks`
--

CREATE TABLE `framworks` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `framworks`
--

INSERT INTO `framworks` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Laravel ', '2019-07-09 02:58:21.310017', '2019-07-09 02:58:21.310017'),
(2, 'Spring', '2019-07-09 02:58:29.240874', '2019-07-09 02:58:29.240874'),
(3, 'Rails', '2019-07-09 02:58:38.219806', '2019-07-09 02:58:38.219806'),
(4, 'React JS', '2019-07-09 02:58:59.568052', '2019-07-09 02:58:59.568052'),
(5, 'Node JS', '2019-07-09 02:59:06.660760', '2019-07-09 02:59:06.660760'),
(6, 'Cake PHP', '2019-07-09 02:59:22.089256', '2019-07-09 02:59:22.089256'),
(7, 'Readux', '2019-07-09 02:59:36.305229', '2019-07-09 02:59:36.305229'),
(8, 'Symfony', '2019-07-09 03:00:42.323077', '2019-07-09 03:00:42.323077'),
(9, 'CodeIgniter', '2019-07-09 03:00:53.807938', '2019-07-09 03:00:53.807938'),
(10, 'Yii 2', '2019-07-09 03:01:04.263628', '2019-07-09 03:01:04.263628'),
(11, 'Phalcon', '2019-07-09 03:01:18.613616', '2019-07-09 03:01:18.613616'),
(12, 'Zend Framework', '2019-07-09 03:01:30.353946', '2019-07-09 03:01:30.353946'),
(13, 'Slim', '2019-07-09 03:01:41.856335', '2019-07-09 03:01:41.856335'),
(14, 'FuelPHP', '2019-07-09 03:01:52.549619', '2019-07-09 03:01:52.549619'),
(15, 'PHPixie', '2019-07-09 03:02:04.400502', '2019-07-09 03:02:04.400502');

-- --------------------------------------------------------

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `positions`
--

INSERT INTO `positions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Leader ', '2019-07-09 03:02:38.337136', '2019-07-09 03:02:38.337136'),
(2, 'IT helpdesk', '2019-07-09 03:02:47.621297', '2019-07-09 03:02:47.621297'),
(3, 'Tester', '2019-07-09 03:03:07.203425', '2019-07-09 03:03:07.203425'),
(4, 'Product Owner', '2019-07-09 03:03:16.542000', '2019-07-09 03:03:16.542000'),
(5, 'Developer', '2019-07-09 03:03:27.858280', '2019-07-09 03:03:27.858280');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `framwork_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `team_id`, `framwork_id`, `created_at`, `updated_at`) VALUES
(1, 'House management', 1, 4, '2019-07-09 03:05:17.381685', '2019-07-09 03:05:17.381685'),
(2, 'Paw ', 1, 2, '2019-07-09 03:06:53.563843', '2019-07-09 03:06:53.563843'),
(3, 'Han Vlog', 2, 7, '2019-07-09 03:07:10.884208', '2019-07-09 03:07:10.884208'),
(4, 'Flash app', 12, 11, '2019-07-09 03:07:36.732758', '2019-07-09 03:07:48.766069'),
(5, 'Hotel management', 3, 9, '2019-07-09 03:09:15.080278', '2019-07-09 03:09:15.080278'),
(6, 'Game mobile', 4, 12, '2019-07-09 03:10:05.403774', '2019-07-09 03:10:05.403774'),
(7, 'Super computer', 5, 8, '2019-07-09 03:10:35.739421', '2019-07-09 03:10:35.739421'),
(8, 'Protect animal', 5, 7, '2019-07-09 03:11:06.130751', '2019-07-09 03:11:06.130751'),
(9, 'Reapra management', 5, 9, '2019-07-09 03:11:29.349594', '2019-07-09 03:11:29.349594'),
(10, 'PNV ', 7, 6, '2019-07-09 03:11:45.360903', '2019-07-09 03:11:45.360903'),
(11, 'Super man', 7, 1, '2019-07-09 03:12:04.226728', '2019-07-09 03:12:04.226728'),
(12, 'Ba Tan Vlog', 8, 2, '2019-07-09 03:12:31.868880', '2019-07-09 03:12:31.868880'),
(13, 'Winner', 9, 3, '2019-07-09 03:13:09.779068', '2019-07-09 03:13:09.779068'),
(14, 'Internship', 10, 3, '2019-07-09 03:13:40.800909', '2019-07-09 03:13:40.800909');

-- --------------------------------------------------------

--
-- Table structure for table `schema_migrations`
--

CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schema_migrations`
--

INSERT INTO `schema_migrations` (`version`) VALUES
('20190709012613'),
('20190709012715'),
('20190709012914'),
('20190709021543'),
('20190709021600'),
('20190709021647'),
('20190709021855'),
('20190709022008');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'C', '2019-07-09 02:52:49.029873', '2019-07-09 02:52:49.029873'),
(2, 'C++', '2019-07-09 02:52:56.962211', '2019-07-09 02:52:56.962211'),
(3, 'C#', '2019-07-09 02:53:03.552307', '2019-07-09 02:53:03.552307'),
(4, 'PHP', '2019-07-09 02:53:12.967552', '2019-07-09 02:53:12.967552'),
(5, 'React JS', '2019-07-09 02:53:27.939124', '2019-07-09 02:53:27.939124'),
(6, 'Java', '2019-07-09 02:53:57.491080', '2019-07-09 02:53:57.491080'),
(7, '.NET', '2019-07-09 02:54:08.348319', '2019-07-09 02:54:08.348319'),
(8, 'Unity 3D', '2019-07-09 02:54:26.871309', '2019-07-09 02:54:26.871309'),
(9, 'MySQL', '2019-07-09 02:54:35.058471', '2019-07-09 02:54:35.058471'),
(10, 'Ruby On Rails', '2019-07-09 02:54:55.200980', '2019-07-09 02:54:55.200980');

-- --------------------------------------------------------

--
-- Table structure for table `staffs`
--

CREATE TABLE `staffs` (
  `id` bigint(20) NOT NULL,
  `team_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `gender` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `introduction` text,
  `feeling` text,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL,
  `position_id` int(11) NOT NULL,
  `image` varchar(225) NOT NULL DEFAULT 'image1.png'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staffs`
--

INSERT INTO `staffs` (`id`, `team_id`, `name`, `email`, `phone`, `gender`, `address`, `introduction`, `feeling`, `created_at`, `updated_at`, `position_id`, `image`) VALUES
(1, 1, 'Minh Phuong', 'phuong@gmail.com', '0123654789', 'Male', '101B Le Huu Trac', 'I love animal, game. I can eat anything', 'I love animal, game. I can eat anything', '2019-07-09 03:23:02.958982', '2019-07-09 03:23:02.958982', 5, 'image1.png'),
(2, 1, 'Thien Phuc', 'phuc@gmail.com', '0123654745', 'Male', '101B Le Huu Trac', 'I love animal, game. I can eat anything', 'I love animal, game. I can eat anything', '2019-07-09 03:23:35.654528', '2019-07-09 03:23:35.654528', 5, 'image1.png'),
(3, 2, 'Thien Thu', 'phuc1@gmail.com', '0123654469', 'Male', '101B Le Huu Trac', 'I love animal, game. I can eat anything', 'I love animal, game. I can eat anything', '2019-07-09 03:24:22.188956', '2019-07-09 03:24:22.188956', 5, 'image1.png'),
(4, 2, 'Thuy PHuong', 'thuy@gmail.com', '0123654781', 'Male', '101B Le Huu Trac', 'I love animal, game. I can eat anything', 'I love animal, game. I can eat anything', '2019-07-09 03:25:38.675099', '2019-07-09 03:25:38.675099', 5, 'image1.png'),
(5, 3, 'Thay Ba', 'ba@gmail.com', '0165854469', 'Female', '99 To Hien Thanh', 'I love animal, game. I can eat anything', 'vI love animal, game. I can eat anything', '2019-07-09 03:26:21.324680', '2019-07-09 03:26:21.324680', 5, 'image1.png'),
(6, 4, 'Quang Hai', 'hai@gmail.com', '0123655545', 'Female', '101B Le Huu Trac', 'I love animal, game. I can eat anything', 'I love animal, game. I can eat anything', '2019-07-09 03:43:49.562777', '2019-07-09 03:43:49.562777', 5, 'image1.png'),
(7, 6, 'Thien Phuc 1', 'phuong11@gmail.com', '0123654789', 'Male', '101B Le Huu Trac', 'staff_managerment_development', 'staff_managerment_development', '2019-07-09 04:12:37.335467', '2019-07-09 04:12:37.335467', 4, 'image1.png'),
(8, 7, 'Quang tai', 'tai@gmail.com', '0123654781', 'Male', '101B Le Huu Trac', 'staff_managerment_development', 'staff_managerment_development', '2019-07-09 04:13:26.967662', '2019-07-09 04:13:26.967662', 2, 'image1.png'),
(9, 8, 'Tieu Bao', 'phuc16@gmail.com', '0123654781', 'Male', '101B Le Huu Trac', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', '2019-07-09 04:14:54.477398', '2019-07-09 04:14:54.477398', 4, 'image1.png'),
(10, 5, 'Whatever', 'thuy6@gmail.com', '0123654781', 'Male', '101B Le Huu Trac', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', '2019-07-09 04:15:36.545304', '2019-07-09 04:15:36.545304', 5, 'image1.png'),
(11, 9, 'Thien Phuc 5', 'phuong21@gmail.com', '0123654789', 'Male', '99 To Hien Thanh', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', '2019-07-09 04:15:56.438816', '2019-07-09 04:15:56.438816', 5, 'image1.png'),
(12, 10, 'Minh Phuong 4', 'phuong14@gmail.com', '0123654789', 'Male', '101B Le Huu Trac', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', '2019-07-09 04:16:20.971355', '2019-07-09 04:16:20.971355', 5, 'image1.png'),
(13, 11, 'Van Kieu', 'van@gmail.com', '01236544693', 'Male', '101B Le Huu Trac', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', '2019-07-09 04:16:53.088303', '2019-07-09 04:16:53.088303', 5, 'image1.png'),
(14, 12, 'Phong Tran', 'phong@gmail.com', '0123654781', 'Female', '5 Le Loi', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', '2019-07-09 04:17:26.841060', '2019-07-09 04:17:26.841060', 5, 'image1.png'),
(15, 13, 'ngo Khong', 'khong@gmail.com', '0123654469', 'Female', '5 Le Loi', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', 'vI can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', '2019-07-09 04:17:56.622688', '2019-07-09 04:17:56.622688', 5, 'image1.png'),
(16, 14, 'Dao Nguyen', 'Dao@gmail.com', '0123654745', 'Male', '101B Le Huu Trac', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', '2019-07-09 04:18:25.468538', '2019-07-09 04:18:25.468538', 4, 'image1.png'),
(17, 15, 'Thong Thong', 'thong@gmail.com', '0123654745', 'Male', '101B Le Huu Trac', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', '2019-07-09 04:19:39.976359', '2019-07-09 04:19:39.976359', 5, 'image1.png'),
(26, 9, 'Hoa Nguyen', 'phuonghoa@gmail.com', '0123694781', 'Female', '101B Le Huu Trac', 'uk', 'uk', '2019-07-09 08:39:17.678677', '2019-07-09 08:39:17.678677', 3, 'image1.png'),
(27, 10, '10', 'phuong@gmail.com', '0123654789', 'Male', '101B Le Huu Trac', 'fv', 'fv', '2019-07-09 08:43:33.057056', '2019-07-09 08:43:33.057056', 3, 'image2.jpg'),
(31, 1, '1', 'phuong@gmail.com', '0123654789', 'Male', '101B Le Huu Trac', 'll', 'll', '2019-07-09 09:17:51.001571', '2019-07-09 09:17:51.001571', 1, 'image1.png'),
(34, 4, 'Minh Phuong 22', 'phuong@gmail.com', '0123654789', 'Male', '101B Le Huu Trac', '<span class=\"picture\">\r\n    	<%= f.file_field :picture, accept: \"image/jpeg, image/gif, image/png, image/jpg\" %>\r\n    </span>', '<span class=\"picture\">\r\n    	<%= f.file_field :picture, accept: \"image/jpeg, image/gif, image/png, image/jpg\" %>\r\n    </span>', '2019-07-10 02:05:43.214744', '2019-07-10 02:05:43.214744', 3, 'image1.png'),
(35, 12, 'Quang Minh', 'minh@gmail.com', '01614541454', 'Female', '10121', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', 'I can say that being optimistic is actually vital, (Explain) for the reason that, stress or anxiety is the result of having negative thoughts. So, if you are always positive about life, more likely you have a happy heart, hence you will live a stress-free life.', '2019-07-10 02:11:26.278487', '2019-07-10 02:11:26.278487', 5, 'image1.png');

-- --------------------------------------------------------

--
-- Table structure for table `staff_skills`
--

CREATE TABLE `staff_skills` (
  `id` bigint(20) NOT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `skill_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_skills`
--

INSERT INTO `staff_skills` (`id`, `staff_id`, `skill_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2019-07-09 04:21:37.654062', '2019-07-09 04:21:37.654062'),
(2, 1, 2, '2019-07-09 04:22:45.342500', '2019-07-09 04:22:45.342500'),
(3, 1, 4, '2019-07-09 04:22:56.171088', '2019-07-09 04:22:56.171088'),
(4, 2, 8, '2019-07-09 04:23:04.026156', '2019-07-09 04:23:04.026156'),
(5, 2, 10, '2019-07-09 04:23:10.898619', '2019-07-09 04:23:10.898619'),
(6, 2, 7, '2019-07-09 04:23:17.343080', '2019-07-09 04:23:17.343080'),
(7, 2, 4, '2019-07-09 04:23:23.476895', '2019-07-09 04:23:23.476895'),
(8, 3, 2, '2019-07-09 04:23:34.014696', '2019-07-09 04:23:34.014696'),
(9, 3, 8, '2019-07-09 04:23:40.609541', '2019-07-09 04:23:40.609541'),
(10, 4, 2, '2019-07-09 04:23:46.704200', '2019-07-09 04:23:46.704200'),
(11, 4, 4, '2019-07-09 04:23:55.140111', '2019-07-09 04:23:55.140111'),
(12, 5, 6, '2019-07-09 04:24:02.555290', '2019-07-09 04:24:02.555290'),
(13, 5, 9, '2019-07-09 04:24:08.475208', '2019-07-09 04:24:08.475208'),
(14, 6, 7, '2019-07-09 04:24:13.723024', '2019-07-09 04:24:13.723024'),
(15, 6, 8, '2019-07-09 04:24:18.967009', '2019-07-09 04:24:18.967009'),
(16, 7, 9, '2019-07-09 04:24:25.402270', '2019-07-09 04:24:25.402270'),
(17, 7, 3, '2019-07-09 04:24:30.996315', '2019-07-09 04:24:30.996315'),
(18, 8, 10, '2019-07-09 04:24:42.695296', '2019-07-09 04:24:42.695296'),
(19, 8, 1, '2019-07-09 04:24:50.080156', '2019-07-09 04:24:50.080156'),
(20, 9, 7, '2019-07-09 04:24:55.484821', '2019-07-09 04:24:55.484821'),
(21, 9, 2, '2019-07-09 04:25:00.943448', '2019-07-09 04:25:00.943448'),
(22, 10, 10, '2019-07-09 04:25:07.246503', '2019-07-09 04:25:07.246503'),
(23, 10, 4, '2019-07-09 04:25:12.316560', '2019-07-09 04:25:12.316560'),
(24, 11, 3, '2019-07-09 04:25:17.524034', '2019-07-09 04:25:17.524034'),
(25, 11, 6, '2019-07-09 04:25:26.330109', '2019-07-09 04:25:26.330109'),
(26, 12, 7, '2019-07-09 04:25:32.002139', '2019-07-09 04:25:32.002139'),
(27, 13, 7, '2019-07-09 04:25:39.066402', '2019-07-09 04:25:39.066402'),
(28, 13, 8, '2019-07-09 04:25:49.835691', '2019-07-09 04:25:49.835691'),
(29, 14, 9, '2019-07-09 04:25:57.013652', '2019-07-09 04:25:57.013652'),
(30, 14, 4, '2019-07-09 04:26:02.225446', '2019-07-09 04:26:02.225446'),
(31, 15, 6, '2019-07-09 04:26:08.582230', '2019-07-09 04:26:08.582230'),
(32, 15, 7, '2019-07-09 04:26:14.844595', '2019-07-09 04:26:14.844595');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL,
  `updated_at` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Flash team', '2019-07-09 03:14:09.385900', '2019-07-09 03:14:09.385900'),
(2, 'Ghost', '2019-07-09 03:14:48.164859', '2019-07-09 03:14:48.164859'),
(3, 'Red team', '2019-07-09 03:15:04.755225', '2019-07-09 03:15:04.755225'),
(4, 'Black team', '2019-07-09 03:15:14.992184', '2019-07-09 03:15:14.992184'),
(5, 'PNV team', '2019-07-09 03:15:22.067633', '2019-07-09 03:15:22.067633'),
(6, 'Da Nang', '2019-07-09 03:15:34.582854', '2019-07-09 03:15:34.582854'),
(7, 'Sai Gon', '2019-07-09 03:15:43.001179', '2019-07-09 03:15:43.001179'),
(8, 'Ha Noi', '2019-07-09 03:15:50.121762', '2019-07-09 03:15:50.121762'),
(9, 'China', '2019-07-09 03:16:01.608547', '2019-07-09 03:16:01.608547'),
(10, 'Thay Giao Ba', '2019-07-09 03:16:18.087076', '2019-07-09 03:16:18.087076'),
(11, 'SBTC', '2019-07-09 03:16:24.774662', '2019-07-09 03:16:24.774662'),
(12, 'QG team', '2019-07-09 03:16:38.011554', '2019-07-09 03:16:38.011554'),
(13, 'Gam team', '2019-07-09 03:16:52.226596', '2019-07-09 03:16:52.226596'),
(14, 'BM & SJ', '2019-07-09 03:17:18.690947', '2019-07-09 03:17:18.690947'),
(15, 'HR team', '2019-07-09 03:17:30.155706', '2019-07-09 03:17:30.155706');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acounts`
--
ALTER TABLE `acounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ar_internal_metadata`
--
ALTER TABLE `ar_internal_metadata`
  ADD PRIMARY KEY (`key`);

--
-- Indexes for table `framworks`
--
ALTER TABLE `framworks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `positions`
--
ALTER TABLE `positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schema_migrations`
--
ALTER TABLE `schema_migrations`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `skills`
--
ALTER TABLE `skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staffs`
--
ALTER TABLE `staffs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_skills`
--
ALTER TABLE `staff_skills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acounts`
--
ALTER TABLE `acounts`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `framworks`
--
ALTER TABLE `framworks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `positions`
--
ALTER TABLE `positions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `skills`
--
ALTER TABLE `skills`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `staffs`
--
ALTER TABLE `staffs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `staff_skills`
--
ALTER TABLE `staff_skills`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
