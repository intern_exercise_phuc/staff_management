class AddIndexToAcountUsername < ActiveRecord::Migration[6.0]
  def change
  	add_index :acounts, :username, unique: true
  end
end
