class CreateStaffs < ActiveRecord::Migration[6.0]
  def change
    create_table :staffs do |t|
      t.integer :team_id
      t.string :name
      t.string :email
      t.string :phone
      t.string :gender
      t.string :address
      t.integer :position_id
      t.text :introduction
      t.text :feeling

      t.timestamps
    end
  end
end
