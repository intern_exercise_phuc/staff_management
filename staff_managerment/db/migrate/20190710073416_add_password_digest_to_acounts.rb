class AddPasswordDigestToAcounts < ActiveRecord::Migration[6.0]
  def change
    add_column :acounts, :password_digest, :string
  end
end
