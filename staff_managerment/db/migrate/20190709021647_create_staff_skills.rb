class CreateStaffSkills < ActiveRecord::Migration[6.0]
  def change
    create_table :staff_skills do |t|
      t.integer :staff_id
      t.integer :skill_id

      t.timestamps
    end
  end
end
