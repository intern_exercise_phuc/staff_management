module SessionsHelper
	def log_in(acount)
    	session[:username] = acount.username
    end

	def current_user
	    if session[:username]
	      @current_user ||= Acount.find_by(username: session[:username])
	    end
  	end

  # Returns true if the user is logged in, false otherwise.
    def logged_in?
    	!current_user.nil?
    end

    def log_out
    	session.delete(:username)
    	@current_user = nil
  	end
end
