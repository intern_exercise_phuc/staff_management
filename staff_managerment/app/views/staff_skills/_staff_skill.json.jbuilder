json.extract! staff_skill, :id, :staff_id, :skill_id, :created_at, :updated_at
json.url staff_skill_url(staff_skill, format: :json)
