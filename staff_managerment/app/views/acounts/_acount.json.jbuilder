json.extract! acount, :id, :username, :password, :created_at, :updated_at
json.url acount_url(acount, format: :json)
