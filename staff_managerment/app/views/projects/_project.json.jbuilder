json.extract! project, :id, :name, :team_id, :framwork_id, :created_at, :updated_at
json.url project_url(project, format: :json)
