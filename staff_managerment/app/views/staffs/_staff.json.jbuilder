json.extract! staff, :id, :team_id, :name, :email, :phone, :gender,:image , :position_id, :address, :introduction, :feeling, :created_at, :updated_at
json.url staff_url(staff, format: :json)
