require "administrate/base_dashboard"

class StaffDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    team: Field::HasOne,
    position: Field::HasOne,
    staff_skill: Field::HasOne,
    id: Field::Number,
    team_id: Field::Number,
    name: Field::String,
    email: Field::String,
    phone: Field::String,
    gender: Field::String,
    address: Field::String,
    introduction: Field::Text,
    feeling: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    position_id: Field::Number,
    image: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :team,
    :position,
    :staff_skill,
    :id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :team,
    :position,
    :staff_skill,
    :id,
    :team_id,
    :name,
    :email,
    :phone,
    :gender,
    :address,
    :introduction,
    :feeling,
    :created_at,
    :updated_at,
    :position_id,
    :image,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :team,
    :position,
    :staff_skill,
    :team_id,
    :name,
    :email,
    :phone,
    :gender,
    :address,
    :introduction,
    :feeling,
    :position_id,
    :image,
  ].freeze

  # Overwrite this method to customize how staffs are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(staff)
  #   "Staff ##{staff.id}"
  # end
end
