class FramworksController < ApplicationController
  before_action :set_framwork, only: [:show, :edit, :update, :destroy]
before_action :authenticate_admin!, only: [:edit, :update, :destroy, :new]
  # GET /framworks
  # GET /framworks.json
  def index
    @framworks = Framwork.all
  end

  # GET /framworks/1
  # GET /framworks/1.json
  def show
  end

  # GET /framworks/new
  def new
    @framwork = Framwork.new
  end

  # GET /framworks/1/edit
  def edit
  end

  # POST /framworks
  # POST /framworks.json
  def create
    @framwork = Framwork.new(framwork_params)

    respond_to do |format|
      if @framwork.save
        format.html { redirect_to @framwork, notice: 'Framwork was successfully created.' }
        format.json { render :show, status: :created, location: @framwork }
      else
        format.html { render :new }
        format.json { render json: @framwork.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /framworks/1
  # PATCH/PUT /framworks/1.json
  def update
    respond_to do |format|
      if @framwork.update(framwork_params)
        format.html { redirect_to @framwork, notice: 'Framwork was successfully updated.' }
        format.json { render :show, status: :ok, location: @framwork }
      else
        format.html { render :edit }
        format.json { render json: @framwork.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /framworks/1
  # DELETE /framworks/1.json
  def destroy
    @framwork.destroy
    respond_to do |format|
      format.html { redirect_to framworks_url, notice: 'Framwork was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_framwork
      @framwork = Framwork.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def framwork_params
      params.require(:framwork).permit(:name)
    end
end
