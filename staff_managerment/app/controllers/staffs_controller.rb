class StaffsController < ApplicationController
  before_action :set_staff, only: [:show, :edit, :update, :destroy, :detail]
  before_action :authenticate_admin!, only: [:edit, :update, :destroy, :new]
  def introduction
  end
  # GET /staffs/1/detail
  # GET /staffs/1/detail.json
  def detail
    #sql = ""
    #@staff = ActiveRecord::Base.connection.execute(sql)
    @staffs = Staff.find(1)
  end
  # GET /staffs
  # GET /staffs.json
  def index
    @q = Staff.ransack(params[:q])
    @staffs = @q.result.page(params[:page]).per(8)
    @positions = Position.all
  end

  def page_home
    
  end
  # GET /staffs/1
  # GET /staffs/1.json
  def show
    sql =  "select distinct st.name as name, st.gender, p.name as positions, t.name as team, st.introduction, st.feeling, st.image
          from staffs as st
          left join positions as p on st.position_id = p.id
          inner join teams as t on st.team_id = t.id 
          where st.id = #{params[:id]};
          "
    @staff = ActiveRecord::Base.connection.execute(sql)

    sql2 = "select  p.name as project, f.name as framwork
          from staffs as s 
          inner join teams as t on s.team_id = t.id
          inner join projects as p on t.id = p.team_id
          left join framworks as f on p.framwork_id = f.id
          where s.id = #{params[:id]};"
    @project = ActiveRecord::Base.connection.execute(sql2)      
  end

  # GET /staffs/new
  def new
    @staff = Staff.new
  end

  # GET /staffs/1/edit
  def edit
  end

  # POST /staffs
  # POST /staffs.json
  def create
    @staff = Staff.new(staff_params)

    respond_to do |format|
      if @staff.save
        format.html { redirect_to @staff, notice: 'Staff was successfully created.' }
        format.json { render :show, status: :created, location: @staff }
      else
        format.html { render :new }
        format.json { render json: @staff.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /staffs/1
  # PATCH/PUT /staffs/1.json
  def update
    respond_to do |format|
      if @staff.update(staff_params)
        format.html { redirect_to @staff, notice: 'Staff was successfully updated.' }
        format.json { render :show, status: :ok, location: @staff }
      else
        format.html { render :edit }
        format.json { render json: @staff.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /staffs/1
  # DELETE /staffs/1.json
  def destroy
    @staff.destroy
    respond_to do |format|
      format.html { redirect_to staffs_url, notice: 'Staff was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_staff
      @staff = Staff.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def staff_params
      params.require(:staff).permit(:team_id, :name, :email, :phone, :gender, :position_id, :image ,  :address, :introduction, :feeling)
    end
end
