class SessionsController < ApplicationController
  def new
  end

  def create
    acount = Acount.find_by(username: params[:session][:username].downcase)
    if acount && acount.authenticate(params[:session][:password])
      # Log the user in and redirect to the acount show page.
      
      redirect_to acount
    
    else
      flash[:danger] = 'Invalid email/password combination' # Not quite right!
      render 'new'
    end
  end

  def destroy
  	log_out
    redirect_to root_url
  end
end

