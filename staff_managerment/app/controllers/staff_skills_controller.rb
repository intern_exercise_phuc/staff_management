class StaffSkillsController < ApplicationController
  before_action :set_staff_skill, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!, only: [:edit, :update, :destroy, :new]
  # GET /staff_skills
  # GET /staff_skills.json
  def index
    @staff_skills = StaffSkill.all
  end

  # GET /staff_skills/1
  # GET /staff_skills/1.json
  def show
  end

  # GET /staff_skills/new
  def new
    @staff_skill = StaffSkill.new
  end

  # GET /staff_skills/1/edit
  def edit
  end

  # POST /staff_skills
  # POST /staff_skills.json
  def create
    @staff_skill = StaffSkill.new(staff_skill_params)

    respond_to do |format|
      if @staff_skill.save
        format.html { redirect_to @staff_skill, notice: 'Staff skill was successfully created.' }
        format.json { render :show, status: :created, location: @staff_skill }
      else
        format.html { render :new }
        format.json { render json: @staff_skill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /staff_skills/1
  # PATCH/PUT /staff_skills/1.json
  def update
    respond_to do |format|
      if @staff_skill.update(staff_skill_params)
        format.html { redirect_to @staff_skill, notice: 'Staff skill was successfully updated.' }
        format.json { render :show, status: :ok, location: @staff_skill }
      else
        format.html { render :edit }
        format.json { render json: @staff_skill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /staff_skills/1
  # DELETE /staff_skills/1.json
  def destroy
    @staff_skill.destroy
    respond_to do |format|
      format.html { redirect_to staff_skills_url, notice: 'Staff skill was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_staff_skill
      @staff_skill = StaffSkill.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def staff_skill_params
      params.require(:staff_skill).permit(:staff_id, :skill_id)
    end
end
