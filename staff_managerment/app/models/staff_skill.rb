class StaffSkill < ApplicationRecord
	validates_presence_of :staff_id, :skill_id
	has_many :staff
	has_many :skill
end
