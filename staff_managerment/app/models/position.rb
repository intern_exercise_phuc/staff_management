class Position < ApplicationRecord
	validates_presence_of :name
	has_many :staff
end
