class Staff < ApplicationRecord
	mount_uploader :picture, PictureUploader
	validates_presence_of :name, :email, :phone, :gender, :address, :team_id, :image, :introduction, :feeling, :position_id
	has_one :team
	has_one :position
	has_one :staff_skill
	#include SearchCop
	# def self.search(search)
 #  		if search
 #    		where('name LIKE ?', "%#{search}%")
 #  		else
 #    		scoped
 #  		end
	# end

	# search_scope :search do
 #    attributes all: [:name]  #khai báo tập các field muốn search

 #    options :all, type: :fulltext, default: true #định nghĩa chúng ta muốn sử dụng full-text search
 #  end
end
