class Acount < ApplicationRecord
	validates_presence_of :username, :password
	has_secure_password
	validates :password, presence: true, length: { minimum: 6 }
end
