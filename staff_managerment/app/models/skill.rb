class Skill < ApplicationRecord
	validates_presence_of :name
	has_one :staff
end
