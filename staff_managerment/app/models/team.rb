class Team < ApplicationRecord
	validates_presence_of :name
	has_many :project
	has_many :staff
end
